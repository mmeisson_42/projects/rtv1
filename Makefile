# *************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/08/28 14:28:00 by mmeisson          #+#    #+#              #
#    Updated: 2017/10/31 18:09:28 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			= rtv1

CC				= clang

override CFLAGS	+= -MD -Wall -Werror -Wextra -Ofast

VPATH			= ./srcs/:./srcs/parsers/:./srcs/compute/:./srcs/vector/:\
	./srcs/colors/:./srcs/matrices/



SRCS			= main.c errors.c get_scene_from_file.c mlx_init_lib.c \
				  get_next_line.c mlx_hook.c raytracer.c mlx_put_pixel.c \
				  resolve_equation.c get_distance.c colorize.c rotation.c \
				  spotify.c object_intersect.c

SRCS			+= parse_cone.c parse_cylinder.c parse_plane.c parse_scene.c \
				   parse_sphere.c parse_spot.c

SRCS			+= vect_normalize.c vect_norme.c vect_add.c vect_sub.c \
				   vect_multiply.c vect_divide.c vect_dot.c vect_dot.c \
				   vect_scalar.c

SRCS			+= compute_sphere.c compute_plan.c compute_cylinder.c \
				   compute_cone.c

SRCS			+= norme_sphere.c norme_plan.c norme_cylinder.c norme_cone.c

SRCS			+= color_add.c color_sub.c color_mult.c color_div.c \
				   color_dot.c color_distance.c

SRCS			+= matrice_rotate.c matrice_translate.c matrice_mult.c \
				   matrice_add.c matrice_sub.c

SRCS			+= mlx_hook.c


INCS_PATHS		= ./incs/ ./libft/incs/ ./printf/incs/ ./mlx/
INCS			= $(addprefix -I,$(INCS_PATHS))

OBJS_PATH		= ./.objs/
OBJS_NAME		= $(SRCS:.c=.o)
OBJS			= $(addprefix $(OBJS_PATH), $(OBJS_NAME))

DEPS			= $(OBJS:.o=.d)

LIB_PATHS		= ./libft/ ./printf/
LIBS			= $(addprefix -L,$(LIB_PATHS)) -L./mlx/

LDFLAGS			= $(LIBS) -lft -lftprintf -lmlx -framework OpenGL \
				  -framework AppKit -lpthread


all: $(NAME)


$(NAME): $(OBJS)
	@$(foreach PATHS, $(LIB_PATHS),\
		echo "# # # # # #\n#";\
		echo '# \033[31m' Compiling $(PATHS) '\033[0m';\
		echo "#\n# # # # # #";\
		make -j8 -C $(PATHS);\
	)
	make -j42 -C ./mlx/
	$(CC) $^ -o $@ $(LDFLAGS)

$(OBJS_PATH)%.o: $(SRCS_PATHS)%.c Makefile
	@mkdir -p $(OBJS_PATH)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

clean:
	@$(foreach PATHS, $(LIB_PATHS), make -j8 -C $(PATHS) clean;)
	make -C ./mlx/ clean
	rm -rf $(OBJS_PATH)

fclean:
	@$(foreach PATHS, $(LIB_PATHS), make -j8 -C $(PATHS) fclean;)
	make -C ./mlx/ clean
	rm -rf $(OBJS_PATH)
	rm -f $(NAME)

re: fclean all

-include $(DEPS)
