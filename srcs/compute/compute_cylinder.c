/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute_cylinder.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 15:35:39 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 10:34:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "objects.h"
#include <math.h>

double		compute_cylinder(struct s_object const *object, struct s_ray ray)
{
	struct s_cylinder const	*cy;
	double					op[3];
	double					res;
	t_vc3					l;

	cy = (struct s_cylinder const *)object;
	l = vect_sub(ray.origin, cy->center);
	op[0] = SQUARED(ray.dir.x) + SQUARED(ray.dir.z);
	op[1] = 2 * (ray.dir.x * l.x + ray.dir.z * l.z);
	op[2] = SQUARED(l.x) + SQUARED(l.z) - SQUARED(cy->radius);
	res = resolve_equation(op[0], op[1], op[2]);
	return (res);
}
