/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute_cone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 13:55:19 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 10:46:06 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "compute.h"
#include <math.h>

double	compute_cone(struct s_object const *object, struct s_ray ray)
{
	double		res;
	double		op[3];

	op[0] = 0;
	op[1] = 0;
	op[2] = 0;
	res = resolve_equation(op[0], op[1], op[2]);
	return (res);
	(void)object;
	(void)ray;
}
