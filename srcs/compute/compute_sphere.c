/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute_sphere.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 14:45:05 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 10:33:30 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "objects.h"
#include <math.h>

double		compute_sphere(struct s_object const *object, struct s_ray ray)
{
	struct s_sphere const	*sp;
	double					op[3];
	double					res;
	t_vc3					l;

	sp = (struct s_sphere const *)object;
	l = vect_sub(ray.origin, sp->center);
	op[0] = vect_dot(ray.dir, ray.dir);
	op[1] = 2.0f * vect_dot(ray.dir, l);
	op[2] = vect_dot(l, l) - SQUARED(sp->radius);
	res = resolve_equation(op[0], op[1], op[2]);
	return (res);
}
