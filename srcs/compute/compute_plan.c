/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute_plan.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/28 09:56:10 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 10:34:39 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "objects.h"
#include <math.h>

double		compute_plan(struct s_object const *object, struct s_ray ray)
{
	struct s_plane const	*pl;
	double					denom;

	pl = (struct s_plane *)object;
	denom = vect_dot(pl->normal, ray.dir);
	if (denom > 1e-6)
		return (vect_dot(vect_sub(pl->point, ray.origin), pl->normal) / denom);
	return (-1.0f);
}
