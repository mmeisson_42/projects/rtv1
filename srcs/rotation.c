/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 09:49:36 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 13:28:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objects.h"
#include <math.h>

/*
**	(	1		0		0		)
**	(	0		cos(A)	-sin(A)	)
**	(	0		sin(A)	cos(A)	)
*/

t_vc3	rotation_x(t_vc3 origin, double angle)
{
	double		cosangle;
	double		sinangle;

	cosangle = cos(angle);
	sinangle = sin(angle);
	return ((t_vc3){
			.x = origin.x,
			.y = cosangle * origin.y - sinangle * origin.x,
			.z = sinangle * origin.y + cosangle * origin.z,
	});
}

/*
**	(	cos(A)	0		sin(A)	)
**	(	0		1		0		)
**	(	-sin(A)	0		cos(A)	)
*/

t_vc3	rotation_y(t_vc3 origin, double angle)
{
	double		cosangle;
	double		sinangle;

	cosangle = cos(angle);
	sinangle = sin(angle);
	return ((t_vc3){
			.x = cosangle * origin.x + sinangle * origin.z,
			.y = origin.y,
			.z = -sinangle * origin.x + cosangle * origin.z,
	});
}

/*
**	(	cos(A)	-sin(A)	0		)
**	(	sin(A)	cos(A)	0		)
**	(	0		0		1		)
*/

t_vc3	rotation_z(t_vc3 origin, double angle)
{
	double		cosangle;
	double		sinangle;

	cosangle = cos(angle);
	sinangle = sin(angle);
	return ((t_vc3){
			.x = cosangle * origin.x - sinangle * origin.y,
			.y = sinangle * origin.x + cosangle * origin.y,
			.z = origin.z,
	});
}
