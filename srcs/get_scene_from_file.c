/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_scene_from_file.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 11:46:45 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 13:35:32 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "libft.h"
#include "get_next_line.h"
#include "parsers.h"
#include <fcntl.h>

static void			parse_line(struct s_scene *sc, char **tab)
{
	size_t						i;
	const struct s_parser		parsers[] = {
		{{.name = PLANE}, parse_plane},
		{{.name = SPHERE}, parse_sphere},
		{{.name = CONE}, parse_cone},
		{{.name = CYLINDER}, parse_cylinder},
		{{.name = SCENE}, parse_scene},
		{{.name = SPOT}, parse_spot},
	};

	i = 0;
	while (i < TAB_LEN(parsers))
	{
		if (ft_strequ(tab[0], parsers[i].identifier.name))
		{
			parsers[i].parser(sc, tab);
			return ;
		}
		i++;
	}
	/* May raise an error here */
}

static void			parse_file(int fd, struct s_scene *sc)
{
	int		ret;
	char	*str;
	char	**tab;

	while ((ret = get_next_line(fd, &str)) > 0)
	{
		tab = ft_whitesplit(str);
		if (tab == NULL)
			fatalerror();
		ft_strdel(&str);
		if (ft_tablen(tab) > 0)
			parse_line(sc, tab);
		ft_tabdel(&tab);
	}
	if (ret == -1)
		fatalerror();
}

struct s_scene		get_scene_from_file(const char *file_name)
{
	int				fd;
	struct s_scene	sc;

	ft_bzero(&sc, sizeof(sc));
	fd = open(file_name, O_RDONLY);
	if (fd == -1)
		fatalerror();
	parse_file(fd, &sc);
	close(fd);
	return (sc);
}
