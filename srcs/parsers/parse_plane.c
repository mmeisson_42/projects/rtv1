/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_plane.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 13:12:22 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 13:28:29 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "compute.h"

/*
**	Expected format: PLANE pointX pointY pointZ normalX normalY normalZ color
*/

void		parse_plane(struct s_scene *sc, char **tab)
{
	struct s_plane	pl;
	struct s_list	*new;

	if (ft_tablen(tab) != 8)
		msg_fatalerror("Error while trying to parse a plane");
	ft_bzero(&pl, sizeof(pl));
	pl.compute = compute_plan;
	pl.norme = norme_plan;
	ft_strcpy(pl.identifier.name, PLANE);
	pl.point.x = ft_atod(tab[1]);
	pl.point.y = ft_atod(tab[2]);
	pl.point.z = ft_atod(tab[3]);
	pl.normal.x = ft_atod(tab[4]);
	pl.normal.y = ft_atod(tab[5]);
	pl.normal.z = ft_atod(tab[6]);
	pl.normal = vect_normalize(pl.normal);
	pl.color.integer = ft_atoi(tab[7]);
	new = ft_lstnew(&pl, sizeof(pl));
	if (new == NULL)
		fatalerror();
	ft_lstadd(&sc->objects, new);
}
