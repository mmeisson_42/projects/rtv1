/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_sphere.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 13:13:03 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 13:28:37 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "objects.h"
#include "compute.h"

/*
**	Expected format: SPHERE radius posX posY posZ color
*/

void		parse_sphere(struct s_scene *sc, char **tab)
{
	struct s_sphere		sp;
	struct s_list		*new;

	if (ft_tablen(tab) != 6)
		msg_fatalerror("Error while trying to parse a sphere");
	ft_bzero(&sp, sizeof(sp));
	sp.size = sizeof(sp);
	ft_strcpy(sp.identifier.name, SPHERE);
	sp.radius = ft_atoi(tab[1]);
	sp.center.x = ft_atod(tab[2]);
	sp.center.y = ft_atod(tab[3]);
	sp.center.z = ft_atod(tab[4]);
	sp.color.integer = ft_atoi(tab[5]);
	sp.compute = compute_sphere;
	sp.norme = norme_sphere;
	new = ft_lstnew(&sp, sizeof(sp));
	if (new == NULL)
		fatalerror();
	ft_lstadd(&sc->objects, new);
}
