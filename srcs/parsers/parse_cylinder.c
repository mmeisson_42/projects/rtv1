/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_cylinder.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 13:12:13 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 13:28:25 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "compute.h"

/*
**	Expected format: CYLINDER pointX pointY pointZ dirX dirY dirZ radius color
*/

void		parse_cylinder(struct s_scene *sc, char **tab)
{
	struct s_cylinder	cy;
	struct s_list		*new;

	if (ft_tablen(tab) != 9)
		msg_fatalerror("Error while trying to parse a cylinder");
	ft_bzero(&cy, sizeof(cy));
	cy.compute = compute_cylinder;
	cy.norme = norme_cylinder;
	cy.size = sizeof(cy);
	ft_strcpy(cy.identifier.name, CYLINDER);
	cy.center.x = ft_atod(tab[1]);
	cy.center.y = ft_atod(tab[2]);
	cy.center.z = ft_atod(tab[3]);
	cy.dir.x = ft_atod(tab[4]);
	cy.dir.y = ft_atod(tab[5]);
	cy.dir.z = ft_atod(tab[6]);
	cy.dir = vect_normalize(cy.dir);
	cy.radius = ft_atoi(tab[7]);
	cy.color.integer = ft_atoi(tab[8]);
	new = ft_lstnew(&cy, sizeof(cy));
	if (new == NULL)
		fatalerror();
	ft_lstadd(&sc->objects, new);
}
