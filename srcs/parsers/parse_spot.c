/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_spot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 13:13:16 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 13:28:46 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

/*
**	Expected format: SPOT pos.x pos.y pos.z color intensity
*/

void		parse_spot(struct s_scene *sc, char **tab)
{
	struct s_spot		sp;
	struct s_list		*new;

	if (ft_tablen(tab) != 6)
		msg_fatalerror("Error while trying to parse a spot");
	ft_bzero(&sp, sizeof(sp));
	sp.size = sizeof(sp);
	ft_strcpy(sp.identifier.name, SPOT);
	sp.pos.x = ft_atod(tab[1]);
	sp.pos.y = ft_atod(tab[2]);
	sp.pos.z = ft_atod(tab[3]);
	sp.light.color.integer = ft_atoi(tab[4]);
	sp.light.intensity = ft_atoi(tab[5]);
	new = ft_lstnew(&sp, sizeof(sp));
	if (new == NULL)
		fatalerror();
	ft_lstadd(&sc->spots, new);
}
