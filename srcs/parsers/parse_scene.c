/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_scene.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 13:12:41 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 13:28:33 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

/*
** Expected format SCENE ambient_intensity amb_color eyeX eyeY eyeZ dirX
** dirY dirZ focal
*/

void		parse_scene(struct s_scene *sc, char **tab)
{
	if (ft_tablen(tab) != 10)
		msg_fatalerror("Error while trying to parse scene");
	if (sc->defined == true)
		msg_fatalerror("Error, scene defined twice or more");
	sc->defined = true;
	sc->ambient.intensity = ft_atoi(tab[1]);
	sc->ambient.color.integer = ft_atoi(tab[2]);
	sc->eye.x = ft_atod(tab[3]);
	sc->eye.y = ft_atod(tab[4]);
	sc->eye.z = ft_atod(tab[5]);
	sc->dir.x = ft_atod(tab[6]);
	sc->dir.y = ft_atod(tab[7]);
	sc->dir.z = ft_atod(tab[8]);
	sc->focal = ft_atod(tab[9]);
}
