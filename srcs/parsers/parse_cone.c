/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_cone.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 13:11:33 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/04 13:59:12 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "compute.h"

/*
**	Expected : CONE posX posY posZ angle(rad) color
*/

void		parse_cone(struct s_scene *sc, char **tab)
{
	struct s_cone		co;
	struct s_list		*new;

	if (ft_tablen(tab) != 6)
		msg_fatalerror("Error while trying to parse a cylinder");
	ft_bzero(&co, sizeof(co));
	co.compute = compute_cone;
	ft_strcpy(co.identifier.name, CONE);
	co.pos.x = ft_atod(tab[1]);
	co.pos.y = ft_atod(tab[2]);
	co.pos.z = ft_atod(tab[3]);
	co.angle = ft_atod(tab[4]);
	co.color.integer = ft_atoi(tab[5]);
	new = ft_lstnew(&co, sizeof(co));
	if (new == NULL)
		fatalerror();
	ft_lstadd(&sc->objects, new);
}
