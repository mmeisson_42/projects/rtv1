/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raytracer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 13:53:45 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 10:45:44 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "objects.h"
#include "mlx.h"
#include <stdint.h>
#include <math.h>
#include <pthread.h>
#include "threads.h"

static inline t_pt3	get_direction(struct s_scene *sc, t_pt3 screen)
{
	t_vc3			dir;

	dir.x = sc->dir.x + screen.x - (SIZE_X / 2.0f);
	dir.y = sc->dir.y + screen.y - (SIZE_Y / 2.0f);
	dir.z = sc->dir.z + sc->focal;
	return (vect_normalize(vect_add(dir, sc->eye)));
}

/*
**	min_y is implicit -> max_y - (SIZE_Y / MAX_THREAD)
*/

static void			raytrace(struct s_scene *sc, struct s_graphic *mlx,
		uint64_t max_y)
{
	t_pt3			screen;
	struct s_ray	ray;
	struct s_hit	hit;
	union u_color	color;

	screen.y = max_y - (SIZE_Y / MAX_THREADS);
	ray.origin = sc->eye;
	while (screen.y < max_y)
	{
		screen.x = 0;
		while (screen.x < SIZE_X)
		{
			ray.dir = get_direction(sc, screen);
			hit = get_nearest_object(sc->objects, ray);
			if (hit.distance != -1.0f)
			{
				color = colorize(sc, hit);
				mlx_put_pixel(mlx, screen.x, screen.y, color.integer);
			}
			else
				mlx_put_pixel(mlx, screen.x, screen.y, 0x0);
			screen.x++;
		}
		screen.y++;
	}
}

static void			*raytrace_call(void *param)
{
	raytrace(((void **)param)[0], ((void **)param)[1],
			(uint64_t)((void **)param)[2]);
	return (NULL);
}

void				raytracer(struct s_scene *sc, struct s_graphic *mlx)
{
	uint64_t			i;
	struct s_threads	threads[MAX_THREADS];

	i = 0;
	while (i < MAX_THREADS)
	{
		threads[i].param[0] = sc;
		threads[i].param[1] = mlx;
		threads[i].param[2] = (void *)((i + 1) * (SIZE_Y / MAX_THREADS));
		pthread_create(&threads[i].id, 0, raytrace_call, threads[i].param);
		i++;
	}
	i = 0;
	while (i < MAX_THREADS)
	{
		pthread_join(threads[i].id, NULL);
		i++;
	}
	mlx_put_image_to_window(mlx->mlx, mlx->win, mlx->img, 0, 0);
}
