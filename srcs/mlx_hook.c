/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_hook.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 13:14:59 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 11:03:05 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "objects.h"
#include "mlx.h"
#include "mlx_keys.h"
#include <stdbool.h>

static struct s_object	*selected = NULL;

int		mouse_handler(int mouse_code, int x, int y, void *const param)
{
	struct s_scene		*sc;
	struct s_graphic	*mlx;
	struct s_hit		hit;
	struct s_ray		ray;

	sc = ((void **)param)[0];
	mlx = ((void **)param)[1];
	ray.origin = sc->eye;
	ray.dir.x = sc->dir.x + x - (SIZE_X / 2.0f);
	ray.dir.y = sc->dir.y + y - (SIZE_Y / 2.0f);
	ray.dir.z = sc->dir.x + sc->focal;
	ray.dir = vect_normalize(vect_add(ray.origin, ray.dir));
	hit = get_nearest_object(sc->objects, ray);
	selected = hit.object;
	if (hit.object)
		spotify(sc, hit, hit.object->norme(hit.object, hit.coord));
	return (1);
	(void) mouse_code;
}

void	move(struct s_object *object, char c, char op_c)
{
	int		op;

	if (op_c == '+')
		op = 10;
	else if (op_c == '-')
		op = -10;
	else
		op = 0;
	if (c == 'x')
		object->pos.x += op;
	else if (c == 'y')
		object->pos.y += op;
	else if (c == 'y')
		object->pos.z += (op * 100);
}

int		key_handler(int keycode, void *const param)
{
	struct s_scene				*sc;
	struct s_graphic			*mlx;
	static struct s_list		*objects;
	static char					op = 'x';
	static char					op_c = '+';

	sc = ((void **)param)[0];
	mlx = ((void **)param)[1];
	(void)objects;
	if (keycode == MLX_ESCAPE)
		exit(0);
	else if (selected)
	{
		if (keycode == MLX_PLUS)
			op_c = '+';
		else if (keycode == MLX_MINUS)
			op_c = '-';
		else if (keycode == MLX_X)
			op = 'x';
		else if (keycode == MLX_Y)
			op = 'y';
		else if (keycode == MLX_Z)
			op = 'z';
		else
			return (0);
		move(selected, op, op_c);
		raytracer(sc, mlx);
	}
	return (0);
}
