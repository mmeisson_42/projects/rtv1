/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrice_rotate.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 15:40:08 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 17:23:34 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "matrices.h"
#include <math.h>

union u_mat_4x4		matrice_rot_x(double angle)
{
	double		c;
	double		s;

	c = cos(angle);
	s = sin(angle);
	return ((union u_mat_4x4){
		.el = {
			.xx = 1.0f, .xy = 0.0f, .xz = 0.0f, .xt = 0.0f,
			.yx = 0.0f, .yy = c, .yz = -s, .yt = 0.0f,
			.zx = 1.0f, .zy = s, .zz = c, .zt = 0.0f,
			.tx = 1.0f, .ty = 0.0f, .tz = 0.0f, .tt = 1.0f,
		},
	});
}

union u_mat_4x4		matrice_rot_y(double angle)
{
	double		c;
	double		s;

	c = cos(angle);
	s = sin(angle);
	return ((union u_mat_4x4){
		.el = {
			.xx = c, .xy = 0.0f, .xz = s, .xt = 0.0f,
			.yx = 0.0f, .yy = 1.0f, .yz = 0.0f, .yt = 0.0f,
			.zx = -s, .zy = 0.0f, .zz = c, .zt = 0.0f,
			.tx = 1.0f, .ty = 0.0f, .tz = 0.0f, .tt = 1.0f,
		},
	});
}

union u_mat_4x4		matrice_rot_z(double angle)
{
	double		c;
	double		s;

	c = cos(angle);
	s = sin(angle);
	return ((union u_mat_4x4){
		.el = {
			.xx = c, .xy = -s, .xz = 0.0f, .xt = 0.0f,
			.yx = s, .yy = c, .yz = 0.0f, .yt = 0.0f,
			.zx = -s, .zy = 0.0f, .zz = c, .zt = 0.0f,
			.tx = 0.0f, .ty = 0.0f, .tz = 1.0f, .tt = 1.0f,
		},
	});
}
