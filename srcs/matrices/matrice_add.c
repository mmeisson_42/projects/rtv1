/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrice_add.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 16:27:23 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 16:29:58 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "matrices.h"
#include <stddef.h>

union u_mat_4x4		matrice_add(union u_mat_4x4 lmat, union u_mat_4x4 rmat)
{
	size_t				i;
	union u_mat_4x4		res;

	i = 0;
	while (i < 16)
	{
		res.arr[i] = lmat.arr[i] + rmat.arr[i];
		i++;
	}
	return (res);
}
