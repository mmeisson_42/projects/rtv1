/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrice_translate.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 15:38:19 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 16:22:09 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "matrices.h"

union u_mat_4x4		matrice_translate(t_vc3 translate)
{
	return ((union u_mat_4x4){
		.el = {
			.xx = 0.0f, .xy = 0.0f, .xz = 0.0f, .xt = translate.x,
			.yx = 0.0f, .yy = 0.0f, .yz = 0.0f, .yt = translate.y,
			.zx = 0.0f, .zy = 0.0f, .zz = 0.0f, .zt = translate.z,
			.tx = 0.0f, .ty = 0.0f, .tz = 0.0f, .tt = 1.0f,
		},
	});
}
