/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrice_mult.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 16:23:28 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 16:50:26 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "matrices.h"
#include "libft.h"
#include <stddef.h>

union u_mat_4x4		matrice_mult(union u_mat_4x4 lmat, union u_mat_4x4 rmat)
{
	size_t				i;
	size_t				j;
	size_t				k;
	double				sum;
	union u_mat_4x4		res;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			sum = 0;
			k = 0;
			while (k < 4)
			{
				sum += lmat.d_arr[k][j] * rmat.d_arr[i][k];
				k++;
			}
			res.d_arr[i][j] = sum;
			j++;
		}
		i++;
	}
	return (res);
}
