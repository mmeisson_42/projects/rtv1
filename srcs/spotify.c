/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spotify.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 10:07:31 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 10:55:55 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "objects.h"
#include "colors.h"

static void		update_shaded_color(union u_color *color, struct s_spot *spot,
		t_vc3 dir, t_vc3 obj_norme)
{
	double				shade;

	shade = MAX(0.0f, vect_dot(obj_norme, dir));
	if (shade)
		*color = color_add(*color, color_mult(spot->light.color, shade));
}

/*
**	Remember  : spots already shaded with intensity
*/

union u_color	spotify(struct s_scene *sc, struct s_hit hit, t_vc3 obj_norme)
{
	union u_color		res;
	struct s_list		*spots;
	struct s_spot		*spot;
	struct s_ray		ray;
	double				spot_distance;

	spots = sc->spots;
	res.integer = 0;
	ray.origin = hit.coord;
	while (spots)
	{
		spot = spots->content;
		ray.dir = vect_normalize(vect_sub(spot->pos, ray.origin));
		spot_distance = get_distance(spot->pos, ray.origin);
		if (object_intersect(hit.object, sc->objects, ray,
					spot_distance) == false)
			update_shaded_color(&res, spot, ray.dir, obj_norme);
		spots = spots->next;
	}
	return (res);
}
