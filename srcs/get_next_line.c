/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 19:52:00 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/10 14:45:37 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static t_file	*search_by_fd(const int fd, t_file **b_list)
{
	if (!*b_list)
	{
		*b_list = ft_memalloc(sizeof(t_file));
		(*b_list)->fd = fd;
		(*b_list)->i = 0;
		return (*b_list);
	}
	if ((*b_list)->fd == fd)
		return (*b_list);
	return (search_by_fd(fd, &(*b_list)->next));
}

static int		load_and_stop(t_file *c_file, char **str, int tem)
{
	if (c_file->buffer[c_file->i] == '\n')
	{
		c_file->buffer[c_file->i++] = '\0';
		*str = ft_strover(*str, c_file->buffer);
		ft_memmove(c_file->buffer, &(c_file->buffer[c_file->i]),
				BUFF_SIZE - c_file->i);
		ft_bzero((&(c_file->buffer[BUFF_SIZE - c_file->i])), c_file->i);
		c_file->i = (tem) ? BUFF_SIZE - c_file->i : 0;
		return (1);
	}
	if (c_file->buffer[c_file->i] == '\0' || c_file->buffer[c_file->i] == EOF)
	{
		c_file->buffer[c_file->i] = '\0';
		*str = ft_strover(*str, c_file->buffer);
		ft_bzero(c_file->buffer, BUFF_SIZE);
		c_file->i = 0;
		return (0);
	}
	return (-1);
}

int				get_next_line(const int fd, char **str)
{
	static t_file	*l_file = NULL;
	t_file			*c_file;
	int				tem;

	if (!str || fd < 0)
		return (-1);
	*str = NULL;
	c_file = search_by_fd(fd, &l_file);
	while ((tem = read(fd, c_file->buffer + c_file->i,
					BUFF_SIZE - c_file->i)) != -1)
	{
		c_file->i = 0;
		while (c_file->i < BUFF_SIZE)
		{
			if (c_file->buffer[c_file->i] == '\0' ||
					c_file->buffer[c_file->i] == EOF ||
						c_file->buffer[c_file->i] == '\n')
				return (load_and_stop(c_file, str, tem));
			(c_file->i)++;
		}
		*str = ft_strover(*str, c_file->buffer);
		ft_bzero(c_file->buffer, BUFF_SIZE);
		c_file->i = 0;
	}
	return (-1);
}
