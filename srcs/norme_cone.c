/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   norme_cone.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 14:35:14 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/22 18:01:05 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "compute.h"

t_vc3	norme_cone(struct s_object const *object, t_pt3 hit)
{
	struct s_cone const	*co;

	co = (struct s_cone const *)object;
	return ((t_vc3){
			.x = 0,
			.y = 0,
			.z = 0,
	});
	(void)hit;
}
