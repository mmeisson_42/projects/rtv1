/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colorize.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/29 16:17:53 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 11:06:58 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "colors.h"
#include <math.h>

static union u_color	shift_color(union u_color color, union u_color light)
{
	uint32_t		col[4];

	col[0] = (uint32_t)(color.argb[0] * (light.argb[0] / 255.0f));
	col[1] = (uint32_t)(color.argb[1] * (light.argb[1] / 255.0f));
	col[2] = (uint32_t)(color.argb[2] * (light.argb[2] / 255.0f));
	col[3] = (uint32_t)(color.argb[3] * (light.argb[3] / 255.0f));
	color.argb[0] = (unsigned char)MIN(0xff, col[0]);
	color.argb[1] = (unsigned char)MIN(0xff, col[1]);
	color.argb[2] = (unsigned char)MIN(0xff, col[2]);
	color.argb[3] = (unsigned char)MIN(0xff, col[3]);
	return (color);
}

/*
**	Remember  : sc->ambient already shaded with intensity
*/

union u_color			colorize(struct s_scene *sc, struct s_hit hit)
{
	union u_color		sight;
	t_vc3				obj_norme;

	obj_norme = hit.object->norme(hit.object, hit.coord);
	sight = spotify(sc, hit, obj_norme);
	sight = color_add(sight, sc->ambient.color);
	sight = shift_color(hit.object->color, sight);
	return (sight);
}
