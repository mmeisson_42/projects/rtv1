/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_put_pixel.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 15:35:36 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 13:33:23 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "colors.h"
#include <stdint.h>

static int				is_big_endian(void)
{
	const union u_color	var = {.integer = 1};

	return ((int)var.argb[0]);
}

static inline uint32_t	swap_bytes(uint32_t data)
{
	return ((data >> 24) & 0xff) |
	((data << 8) & 0xff0000) |
	((data >> 8) & 0xff00) |
	((data << 24) & 0xff000000);
}

void					mlx_put_pixel(struct s_graphic *mlx, int32_t x,
		int32_t y, uint32_t color)
{
	static int		reverse = -1;

	if (reverse == -1)
		reverse = is_big_endian() == mlx->endian;
	if (reverse)
		color = swap_bytes(color);
	if (x >= 0 && x < SIZE_X && y >= 0 && y < SIZE_Y)
		mlx->data[y * SIZE_X + x].integer = color;
}
