/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_init_lib.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 13:08:42 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/23 15:11:29 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "mlx.h"

struct s_graphic	mlx_init_lib(void *param)
{
	struct s_graphic	mlx;

	ft_bzero(&mlx, sizeof(mlx));
	mlx.mlx = mlx_init();
	if (mlx.mlx == NULL)
		fatalerror();
	mlx.win = mlx_new_window(mlx.mlx, SIZE_X, SIZE_Y, TITLE);
	if (mlx.win == NULL)
		fatalerror();
	mlx.img = mlx_new_image(mlx.mlx, SIZE_X, SIZE_Y);
	if (mlx.img == NULL)
		fatalerror();
	mlx.data = (union u_color *)mlx_get_data_addr(mlx.img, &mlx.bpp,
			&mlx.size_line, &mlx.endian);
	if (mlx.data == NULL)
		fatalerror();
	mlx_key_hook(mlx.win, key_handler, param);
	mlx_mouse_hook(mlx.win, mouse_handler, param);
	return (mlx);
}
