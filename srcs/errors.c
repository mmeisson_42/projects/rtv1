/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 12:02:03 by mmeisson          #+#    #+#             */
/*   Updated: 2017/09/26 12:08:04 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "libft.h"

void		fatalerror(void)
{
	perror("");
	exit(EXIT_FAILURE);
}

void		msg_fatalerror(const char *msg)
{
	extern int		errno;

	ft_putendl_fd(msg, 2);
	if (errno != 0)
		perror("");
	exit(EXIT_FAILURE);
}
