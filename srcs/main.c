/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 12:07:15 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/30 17:10:35 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "mlx.h"
#include <stdlib.h>

void	pre_normalize(struct s_scene *sc)
{
	struct s_list	*spots;
	struct s_spot	*spot;

	sc->ambient.color = color_mult(sc->ambient.color,
			(double)(sc->ambient.intensity % 101) / 100.0f);
	spots = sc->spots;
	while (spots != NULL)
	{
		spot = spots->content;
		spot->light.color = color_mult(spot->light.color,
				(double)(spot->light.intensity % 101) / 100.0f);
		spots = spots->next;
	}
}

int		main(int ac, char **av)
{
	struct s_scene		sc;
	struct s_graphic	mlx;
	const void			*param[2] = {&sc, &mlx};
	extern int			errno;

	if (ac == 2)
	{
		errno = 0;
		sc = get_scene_from_file(av[1]);
		if (sc.defined == false)
			msg_fatalerror("Scene is not defined !");
		mlx = mlx_init_lib(param);
		pre_normalize(&sc);
		raytracer(&sc, &mlx);
		mlx_loop(mlx.mlx);
	}
	return (EXIT_SUCCESS);
}
