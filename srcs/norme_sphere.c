/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   norme_sphere.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 14:25:14 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/26 11:02:35 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "compute.h"

t_vc3	norme_sphere(struct s_object const *object, t_pt3 hit)
{
	struct s_sphere const	*sp;

	sp = (struct s_sphere const *)object;
	return (vect_normalize(vect_sub(hit, sp->center)));
}
