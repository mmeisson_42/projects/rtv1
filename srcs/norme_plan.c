/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   norme_plan.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 14:22:48 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/22 18:01:21 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "compute.h"
#include "objects.h"

t_vc3		norme_plan(struct s_object const *object, t_pt3 hit)
{
	struct s_plane const	*pl;

	pl = (struct s_plane const *)object;
	return (pl->normal);
	(void)hit;
}
