/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vect_scalar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/29 12:03:03 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/17 12:34:11 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vectors.h"

inline t_vc3	vect_scalar(t_vc3 lvalue, t_vc3 rvalue)
{
	return ((t_vc3){
		.x = lvalue.x * rvalue.z,
		.y = lvalue.y * rvalue.y,
		.z = lvalue.z * rvalue.z,
			});
}
