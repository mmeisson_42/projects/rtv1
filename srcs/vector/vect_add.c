/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vect_add.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 15:17:16 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/17 12:33:41 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vectors.h"

inline t_vc3	vect_add(t_vc3 lvalue, t_vc3 rvalue)
{
	return ((t_vc3){
			.x = lvalue.x + rvalue.x,
			.y = lvalue.y + rvalue.y,
			.z = lvalue.z + rvalue.z,
			});
}
