/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vect_multiply.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 15:04:01 by mmeisson          #+#    #+#             */
/*   Updated: 2017/09/27 16:32:33 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vectors.h"
#include <stdint.h>

inline t_vc3	vect_multiply(t_vc3 origin, double multiplier)
{
	return ((t_vc3){
			.x = origin.x * multiplier,
			.y = origin.y * multiplier,
			.z = origin.z * multiplier,
			});
}
