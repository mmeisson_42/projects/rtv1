/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vect_dot.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 15:19:50 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/19 13:51:02 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vectors.h"

inline double		vect_dot(t_vc3 lvalue, t_vc3 rvalue)
{
	return (lvalue.x * rvalue.x +
			lvalue.y * rvalue.y +
			lvalue.z * rvalue.z);
}
