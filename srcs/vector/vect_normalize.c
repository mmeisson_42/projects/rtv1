/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vect_normalize.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 14:51:46 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 11:32:27 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vectors.h"
#include <math.h>

inline t_vc3	vect_normalize(t_vc3 origin)
{
	return (vect_divide(origin, vect_norme(origin)));
}
