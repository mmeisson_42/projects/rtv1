/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vect_norme.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 14:53:23 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/24 15:01:24 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdint.h>
#include "vectors.h"
#include "rt.h"

inline double	vect_norme(t_vc3 vc)
{
	return (sqrt(SQUARED(vc.x) + SQUARED(vc.y) + SQUARED(vc.z)));
}
