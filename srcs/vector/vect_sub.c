/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vect_sub.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 15:14:40 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/17 12:34:17 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vectors.h"

inline t_vc3	vect_sub(t_vc3 lvalue, t_vc3 rvalue)
{
	return ((t_vc3){
			.x = lvalue.x - rvalue.x,
			.y = lvalue.y - rvalue.y,
			.z = lvalue.z - rvalue.z,
			});
}
