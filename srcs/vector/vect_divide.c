/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vect_divide.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 15:01:03 by mmeisson          #+#    #+#             */
/*   Updated: 2017/09/27 16:32:33 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vectors.h"
#include <stdint.h>

inline t_vc3	vect_divide(t_vc3 origin, double divider)
{
	return ((t_vc3){
			.x = origin.x / divider,
			.y = origin.y / divider,
			.z = origin.z / divider,
			});
}
