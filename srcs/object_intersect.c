/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   object_intersect.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 10:02:43 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/01 17:29:11 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "objects.h"
#include "vectors.h"

bool			object_intersect(struct s_object const *curr,
		struct s_list *objects, struct s_ray ray, double spot_distance)
{
	struct s_object	const	*object;
	double					rel_dist;
	struct s_pt3			hit_coord;
	double					distance;

	while (objects)
	{
		if (curr != objects->content)
		{
			object = objects->content;
			rel_dist = object->compute(object, ray);
			if (rel_dist > 0.0f)
			{
				hit_coord = vect_add(ray.origin,
						vect_multiply(ray.dir, rel_dist));
				distance = get_distance(hit_coord, ray.origin);
				if (distance < spot_distance)
					return (true);
			}
		}
		objects = objects->next;
	}
	return (false);
}

struct s_hit	get_nearest_object(struct s_list const *objects,
		struct s_ray ray)
{
	struct s_object		*object;
	struct s_hit		hit;
	double				rel_dist;
	double				distance;
	struct s_pt3		hit_coords;

	hit.object = NULL;
	hit.distance = -1.0f;
	while (objects)
	{
		object = objects->content;
		rel_dist = object->compute(object, ray);
		if (rel_dist != -1.0f)
		{
			hit_coords = vect_add(ray.origin, vect_multiply(ray.dir, rel_dist));
			distance = get_distance(ray.origin, hit_coords);
			if (hit.distance == -1.0f || distance < hit.distance)
			{
				hit.coord = hit_coords;
				hit.distance = distance;
				hit.object = object;
			}
		}
		objects = objects->next;
	}
	return (hit);
}
