/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   norme_cylinder.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 14:28:48 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 13:30:18 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include "compute.h"

t_vc3	norme_cylinder(struct s_object const *object, t_pt3 hit)
{
	struct s_cylinder const	*cy;

	cy = (struct s_cylinder const *)object;
	return (vect_normalize((t_vc3){
			.x = hit.x - cy->center.x,
			.y = 0.0f,
			.z = hit.z - cy->center.z,
	}));
}
