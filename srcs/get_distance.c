/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_distance.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/27 12:47:20 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/30 13:33:04 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"
#include <math.h>

inline double	get_distance(t_pt3 origin, t_pt3 dest)
{
	return (sqrt(
				SQUARED(origin.x - dest.x) +
				SQUARED(origin.y - dest.y) +
				SQUARED(origin.z - dest.z)));
}
