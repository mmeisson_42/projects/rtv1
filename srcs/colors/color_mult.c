/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_mult.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/28 10:54:45 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/26 17:00:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "colors.h"

union u_color	color_mult(union u_color c, double multiplier)
{
	uint32_t		colors[4];

	colors[0] = c.argb[0] * multiplier;
	colors[1] = c.argb[1] * multiplier;
	colors[2] = c.argb[2] * multiplier;
	colors[3] = c.argb[3] * multiplier;
	c.argb[0] = (unsigned char)MIN(0xff, colors[0]);
	c.argb[1] = (unsigned char)MIN(0xff, colors[1]);
	c.argb[2] = (unsigned char)MIN(0xff, colors[2]);
	c.argb[3] = (unsigned char)MIN(0xff, colors[3]);
	return (c);
}
