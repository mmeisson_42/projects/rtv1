/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_distance.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/30 16:20:19 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/02 14:45:44 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "colors.h"

inline union u_color		color_distance(union u_color color,
		double distance)
{
	return (color_div(color, distance * distance / 255));
}
