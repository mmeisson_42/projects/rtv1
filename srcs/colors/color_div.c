/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_div.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/28 10:52:26 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/07 12:22:57 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "colors.h"

union u_color	color_div(union u_color c, double divider)
{
	c.argb[0] /= divider;
	c.argb[1] /= divider;
	c.argb[2] /= divider;
	c.argb[3] /= divider;
	return (c);
}
