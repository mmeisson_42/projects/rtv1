/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_sub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/28 11:01:50 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/22 18:42:55 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "colors.h"

union u_color	color_sub(union u_color lvalue, union u_color rvalue)
{
	uint32_t		colors[4];

	colors[0] = lvalue.argb[0] - rvalue.argb[0];
	colors[1] = lvalue.argb[1] - rvalue.argb[1];
	colors[2] = lvalue.argb[2] - rvalue.argb[2];
	colors[3] = lvalue.argb[3] - rvalue.argb[3];
	lvalue.argb[0] = (unsigned char)MAX(0x00, colors[0]);
	lvalue.argb[1] = (unsigned char)MAX(0x00, colors[1]);
	lvalue.argb[2] = (unsigned char)MAX(0x00, colors[2]);
	lvalue.argb[3] = (unsigned char)MAX(0x00, colors[3]);
	return (lvalue);
}
