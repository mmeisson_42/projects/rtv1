/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolve_equation.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/27 12:45:06 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/30 15:01:30 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdint.h>

double		resolve_equation(double a, double b, double c)
{
	double	discriminant;
	double	res[2];

	discriminant = b * b - (4.0f * a * c);
	if (discriminant == 0)
	{
		return (b / (2.0f * a));
	}
	else if (discriminant > 0)
	{
		discriminant = sqrt(discriminant);
		res[0] = (-b + discriminant) / (2.0f * a);
		res[1] = (-b - discriminant) / (2.0f * a);
		if (res[0] > res[1])
			return (res[0]);
		return (res[1]);
	}
	return (-1.0f);
}
