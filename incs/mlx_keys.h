/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_keys.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/17 15:08:44 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/17 15:12:30 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MLX_KEYS_H
# define MLX_KEYS_H

# define MLX_ESCAPE		53
# define MLX_X			7
# define MLX_Y			16
# define MLX_Z			6
# define MLX_P			35
# define MLX_N			45
# define MLX_PLUS		69
# define MLX_MINUS		78

#endif
