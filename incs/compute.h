/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/27 10:18:17 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 10:38:23 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMPUTE_H
# define COMPUTE_H

# include "rt.h"

double		compute_sphere(struct s_object const *object, struct s_ray ray);
double		compute_plan(struct s_object const *object, struct s_ray ray);
double		compute_cylinder(struct s_object const *object, struct s_ray ray);
double		compute_cone(struct s_object const *object, struct s_ray ray);

t_vc3		norme_sphere(struct s_object const *object, t_pt3 hit);
t_vc3		norme_plan(struct s_object const *object, t_pt3 hit);
t_vc3		norme_cone(struct s_object const *object, t_pt3 hit);
t_vc3		norme_cylinder(struct s_object const *object, t_pt3 hit);

#endif
