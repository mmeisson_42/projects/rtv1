/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsers.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 13:04:10 by mmeisson          #+#    #+#             */
/*   Updated: 2017/09/25 13:11:12 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSERS_H
# define PARSERS_H

# include "rt.h"
# include "get_next_line.h"

# define TAB_LEN(tab) (sizeof(tab) / sizeof(tab[0]))

void		parse_plane(struct s_scene *sc, char **tab);
void		parse_sphere(struct s_scene *sc, char **tab);
void		parse_cone(struct s_scene *sc, char **tab);
void		parse_cylinder(struct s_scene *sc, char **tab);
void		parse_scene(struct s_scene *sc, char **tab);
void		parse_spot(struct s_scene *sc, char **tab);

#endif
