/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/28 11:23:36 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 14:04:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COLORS_H
# define COLORS_H

# include <stdint.h>

# define BIT_SET(var, pos) (var & (1 << pos))

# ifndef MIN
#  define MIN(a, b) (a > b ? b : a)
# endif

# ifndef MAX
#  define MAX(a, b) (a < b ? b : a)
# endif

/*
**	/!\ u_color should be filled in the way an integer would be
**	For the color 0x00ffffff , on a little endian arch, argb[1] should be ff
**	For the color 0x00ffffff , on a big endian arch, argb[2] should be ff
**	And yes, cmp == compute
*/
union			u_color
{
	uint32_t		integer;
	unsigned char	argb[4];
};

union u_color	color_add(union u_color lvalue, union u_color rvalue);
union u_color	color_sub(union u_color lvalue, union u_color rvalue);
union u_color	color_div(union u_color lvalue, double divider);
union u_color	color_mult(union u_color lvalue, double multiplier);
union u_color	color_dot(union u_color lvalue, union u_color rvalue);

union u_color	color_distance(union u_color lvalue, double distance);

#endif
