/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/26 15:05:09 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/25 13:59:56 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTORS_H
# define VECTORS_H

# include <stdint.h>

struct				s_pt3
{
	double		x;
	double		y;
	double		z;
};

typedef struct s_pt3	t_vc3;
typedef struct s_pt3	t_pt3;

t_vc3				vect_divide(t_vc3 origin, double divider);
t_vc3				vect_multiply(t_vc3 origin, double multiplier);
t_vc3				vect_normalize(t_vc3 origin);
double				vect_norme(t_vc3 origin);

t_vc3				vect_sub(t_vc3 lvalue, t_vc3 rvalue);
t_vc3				vect_add(t_vc3 lvalue, t_vc3 rvalue);
double				vect_dot(t_vc3 lvalue, t_vc3 rvalue);
t_vc3				vect_scalar(t_vc3 lvalue, t_vc3 rvalue);

#endif
