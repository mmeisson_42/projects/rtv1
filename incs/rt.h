/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 10:17:53 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 10:59:15 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_H
# define RT_H

# include "objects.h"
# include "libft.h"
# include <stdbool.h>

# define SIZE_X		1080
# define SIZE_Y		800
# define TITLE		"mmeisson :: rtv1"

# define SQUARED(x) ({__typeof__ (x) _x = x; (_x * _x);})

/*
**	Basic structure containers
*/
struct				s_scene
{
	struct s_list	*objects;
	struct s_list	*spots;
	struct s_light	ambient;
	bool			defined;
	bool			drawn;
	t_pt3			eye;
	t_vc3			dir;
	double			focal;
};

struct				s_graphic
{
	void			*mlx;
	void			*win;
	void			*img;
	union u_color	*data;
	int				endian;
	int				bpp;
	int				size_line;
};

/*
**	Used only in get_scene_from_file to parsing purposes
*/
struct				s_parser
{
	union u_id	identifier;
	void		(*parser)(struct s_scene *sc, char **tab);
};

/*
**	Raytracer.c
*/
void				raytracer(struct s_scene *sc, struct s_graphic *mlx);

/*
**	get_scene_from_file.c
*/
struct s_scene		get_scene_from_file(const char *file_name);

/*
**	resolve_equation.c
*/
double				resolve_equation(double a, double b, double c);

/*
**	get_distance.c
*/
double				get_distance(t_pt3 orign, t_pt3 dst);

/*
**	colorize.c
*/
union u_color		colorize(struct s_scene *sc, struct s_hit hit);

/*
**	mlx_init_lib.c
*/
struct s_graphic	mlx_init_lib(void *param);

/*
**	mlx_hook.c
*/
int					key_handler(int keycode, void *param);
int					mouse_handler(int mouse_code, int x, int y, void *param);

/*
**	mlx_put_pixel.c
*/
void				mlx_put_pixel(struct s_graphic *mlx, int32_t x, int32_t y,
		uint32_t color);

/*
**	errors.c
*/
void				fatalerror(void);
void				msg_fatalerror(const char *msg);

/*
**	object_intersect.c
*/
struct s_hit		get_nearest_object(struct s_list const *objects,
		struct s_ray ray);
bool				object_intersect(struct s_object const *curr,
		struct s_list *objects, struct s_ray ray, double spot_distance);

/*
**		spotify.c
*/
union u_color		spotify(struct s_scene *sc, struct s_hit hit,
		t_vc3 obj_norme);

#endif
