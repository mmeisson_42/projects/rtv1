/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrices.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 15:31:36 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 18:09:05 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATRICES_H
# define MATRICES_H

# include "objects.h"

/*
**	a	b	c	d
**	e	f	g	h
**	i	j	k	l
**	m	n	o	p
**	=>
**	xx	yx	zx	tx
**	xy	yy	zy	ty
**	xz	yz	zz	tz
**	xt	yt	zt	tt
*/

union				u_mat_4x4
{
	double			arr[16];
	double			d_arr[4][4];
	struct
	{
		double		xx;
		double		yx;
		double		zx;
		double		tx;

		double		xy;
		double		yy;
		double		zy;
		double		ty;

		double		xz;
		double		yz;
		double		zz;
		double		tz;

		double		xt;
		double		yt;
		double		zt;
		double		tt;
	}				el;
};

union u_mat_4x4		matrice_rot_x(double angle);
union u_mat_4x4		matrice_rot_y(double angle);
union u_mat_4x4		matrice_rot_z(double angle);

union u_mat_4x4		matrice_translate(t_vc3 translate);

union u_mat_4x4		matrice_mult(union u_mat_4x4 lmat, union u_mat_4x4 rmat);
union u_mat_4x4		matrice_add(union u_mat_4x4 lmat, union u_mat_4x4 rmat);
union u_mat_4x4		matrice_sub(union u_mat_4x4 lmat, union u_mat_4x4 rmat);

#endif
