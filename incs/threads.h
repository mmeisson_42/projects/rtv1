/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   threads.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 10:16:16 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 10:54:21 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef THREADS_H
# define THREADS_H

# define MAX_THREADS	16

# include <pthread.h>

struct	s_threads
{
	pthread_t	id;
	void		*param[3];
};

#endif
