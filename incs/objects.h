/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objects.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/21 13:05:26 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 11:03:16 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OBJECTS_H
# define OBJECTS_H

# include <stdint.h>
# include "vectors.h"
# include "colors.h"

# define PLANE		"PLANE"
# define SPHERE		"SPHERE"
# define CONE		"CONE"
# define CYLINDER	"CYLINDER"
# define SCENE		"SCENE"
# define SPOT		"SPOT"

struct s_scene;
struct s_object;

struct		s_ray
{
	t_pt3	origin;
	t_vc3	dir;
};

/*
**	This function's pointer should point on a function that compute and return
**	distance between sc->eye and the hitted object., or -1 of te object is not
**	hitted
*/
typedef double		(*t_cmp)(struct s_object const *object, struct s_ray ray);
/*
**	_
*/

struct s_hit;
typedef t_vc3		(*t_norme)(struct s_object const *, t_pt3);

/*
**	Identify an object
*/

union		u_id
{
	uint64_t	integer;
	char		name[12];
};

struct		s_light
{
	union u_color	color;
	uint32_t		intensity;
};

struct		s_object
{
	uint32_t		size;
	union u_id		identifier;
	t_cmp			compute;
	union u_color	color;
	t_norme			norme;
	t_pt3			pos;
};

/*
**	__
**	Every following object's structures 'inherit' from s_object
*/

struct		s_plane
{
	uint32_t		size;
	union u_id		identifier;
	t_cmp			compute;
	union u_color	color;
	t_norme			norme;
	t_pt3			point;

	t_vc3			normal;
};

struct		s_sphere
{
	uint32_t		size;
	union u_id		identifier;
	t_cmp			compute;
	union u_color	color;
	t_norme			norme;
	struct s_pt3	center;

	int				radius;
};

struct		s_cone
{
	uint32_t		size;
	union u_id		identifier;
	t_cmp			compute;
	union u_color	color;
	t_norme			norme;
	struct s_pt3	pos;

	double			angle;
	int				radius;
};

struct		s_cylinder
{
	uint32_t		size;
	union u_id		identifier;
	t_cmp			compute;
	union u_color	color;
	t_norme			norme;
	t_vc3			dir;
	t_pt3			center;

	int				radius;
};

/*
**	___
*/

struct		s_spot
{
	uint32_t		size;
	union u_id		identifier;
	struct s_pt3	pos;
	struct s_light	light;
};

struct		s_hit
{
	int32_t			distance;
	t_pt3			coord;
	struct s_object	*object;
};

#endif
